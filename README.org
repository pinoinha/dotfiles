# Created 2024-01-31 qua 12:52
#+title: dotfiles
#+options: toc:nil
#+author: pinoinha

dotfiles and scripts for everyday use

* OS

I've been a distro-hopper for a while now. while I use Artix, I'm also interested in gentoo. I also like some BSDs.

- [[https://artixlinux.org][Artix Linux]]
- [[https://voidlinux.org][Void Linux]]
- [[https://www.openbsd.org][OpenBSD]]

* compositors

nowadays I run wayland everywhere I can, and so far I've enjoyed both of these compositors:

- [[https:github.com/riverwm/river][river]]
- [[https:github.com/hyprwm/Hyprland][Hyprland]]

for legacy purposes, these are the X11 window managers I've enjoyed using the most:

- [[http:dwm.suckless.org][dwm]] (check out my [[https://codeberg.org/pinoinha/dwm-meu][fork]])
- [[github:jcs/sdorfehs][sdorfehs]]
