set -gx EDITOR /usr/bin/emacsclient -c -a ''
set -gx VISUAL /usr/bin/emacsclient -c -a ''
set -gx PAGER /usr/bin/bat --plain --terminal-width 72
set -gx TERMINAL /usr/bin/foot
set -gx BROWSER /usr/bin/firefox
set -gx DMENU_CMD /usr/bin/fuzzel -d
set -gx XDG_DATA_HOME "$HOME/.local/share"
set -gx XDG_CACHE_HOME "$HOME/.cache"
set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_STATE_HOME "$HOME/.local/state"

set -gx PASSWORD_STORE_DIR "$XDG_DATA_HOME"/pass

set -gx CARGO_HOME "$XDG_DATA_HOME/cargo"
set -gx CARGO_BIN "$CARGO_HOME/bin"
set -gx RUSTUP_HOME "$XDG_DATA_HOME"/rustup

set -gx GOPATH "$XDG_DATA_HOME/go"
set -gx GOBIN "$GOPATH/bin"

set -gx FZF_DEFAULT_COMMAND 'fd --hidden'

set -gx _JAVA_AWT_WM_NONREPARENTING 1
set -gx _JAVA_OPTIONS -Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java

set -gx ANDROID_HOME "$XDG_DATA_HOME"/android

set -gx ASDF_DATA_DIR "$XDG_DATA_HOME"/asdf

set -gx BASH_COMPLETION_USER_FILE "$XDG_CONFIG_HOME"/bash-completion/bash_completion

set -gx CABAL_DIR "$XDG_DATA_HOME"/cabal
set -gx CABAL_CONFIG "$CABAL_DIR"/config

set -gx CALCHISTFILE "$XDG_CACHE_HOME"/calc_history

set -gx CUDA_CACHE_PATH "$XDG_CACHE_HOME"/nv

set -gx GHCUP_USE_XDG_DIRS true

set -gx JUPYTER_CONFIG_DIR "$XDG_CONFIG_HOME"/jupyter

set -gx LEIN_HOME "$XDG_DATA_HOME"/lein

set -gx LESSHISTFILE "$XDG_STATE_HOME"/less/history

set -gx INPUTRC "$XDG_CONFIG_HOME"/readline/inputrc

set -gx SQLITE_HISTORY "$XDG_CACHE_HOME"/sqlite_history

set -gx WINEPREFIX "$XDG_DATA_HOME"/wine

set -gx XINITRC "$XDG_CONFIG_HOME"/X11/xinitrc

set -gx GRIM_DEFAULT_DIR "(xdg-user-dir PICTURES)/screenshots"

set -gx MOZ_ENABLE_WAYLAND 1

set -gx XKB_DEFAULT_OPTIONS "compose:ralt"

zoxide init fish | source

source /opt/asdf-vm/asdf.fish

abbr -a hx helix

abbr -a pare "paru -Rcs"
abbr -a man /usr/bin/batman

git_aliases

fzf_configure_bindings --directory=\ct --variables= --git_log=\el --git_status=\e\cs --history=\cr --processes=\e\cp

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME
fish_add_path $HOME/.cabal/bin $HOME/.ghcup/bin

if test (tty) = /dev/tty1
    dbus-run-session river
end
