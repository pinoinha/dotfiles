function yda
    yt-dlp -4 -x --audio-format flac \
            -o '~/musica/%(title)s.%(ext)s' \
            --external-downloader aria2c \
            --external-downloader-args '-c -j 3 -x 3 -s 3 -k 1M' \
            $argv
end
