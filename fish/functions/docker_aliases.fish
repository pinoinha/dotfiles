# Defined in /tmp/fish.fHKs5I/docker_aliases.fish @ line 1
function docker_aliases
    set -l dccmd "docker compose"

    abbr -a dco "$dccmd"
    abbr -a dcb "$dccmd build"
    abbr -a dce "$dccmd exec"
    abbr -a dcps "$dccmd ps"
    abbr -a dcrestart "$dccmd restart"
    abbr -a dcrm "$dccmd rm"
    abbr -a dcr "$dccmd run"
    abbr -a dcstop "$dccmd stop"
    abbr -a dcup "$dccmd up"
    abbr -a dcupb "$dccmd up --build"
    abbr -a dcupd "$dccmd up -d"
    abbr -a dcupdb "$dccmd up -d --build"
    abbr -a dcdn "$dccmd down"
    abbr -a dcl "$dccmd logs"
    abbr -a dclf "$dccmd logs -f"
    abbr -a dcpull "$dccmd pull"
    abbr -a dcstart "$dccmd start"
    abbr -a dck "$dccmd kill"
end
