function fish_prompt -d "Manage fish prompt"
    echo (set_color blue)(id  -nu)(set_color red)'@'(set_color magenta)(prompt_hostname) (set_color green)(prompt_pwd)(set_color normal)(set_color yellow) '$ '(set_color normal)
end
